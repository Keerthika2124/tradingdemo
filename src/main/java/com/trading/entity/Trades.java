package com.trading.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Trades {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer tradeid;
	@OneToOne
	private Orders order;
	private Integer tradedQuantity;
	private LocalDate tradeDate;
}
