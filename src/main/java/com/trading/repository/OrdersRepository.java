package com.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trading.entity.Orders;

public interface OrdersRepository extends JpaRepository<Orders, Integer> {

}
