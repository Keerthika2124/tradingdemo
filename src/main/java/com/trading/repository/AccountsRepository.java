package com.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trading.entity.Accounts;

public interface AccountsRepository extends JpaRepository<Accounts, Integer> {

}
